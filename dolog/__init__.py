#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

dolog
-----

Tools to record and document the setup of a server or computer.

"""

from __future__ import absolute_import, division, print_function, \
    with_statement, unicode_literals

from .dolog import Dolog
