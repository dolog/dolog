.. _api:

===
API
===

.. automodule:: dolog
    :members:

    .. autoclass:: Dolog
        :members:
