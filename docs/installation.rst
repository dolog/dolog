============
Installation
============


Stable release
--------------

To install dolog, run this command in your terminal:

.. code-block:: console

    $ pip install dolog

This is the preferred method to install dolog, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for dolog can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/dolog/dolog

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://gitlab.com/dolog/dolog/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Gitlab repo: https://gitlab.com/dolog/dolog
.. _tarball: https://gitlab.com/dolog/dolog/tarball/master
