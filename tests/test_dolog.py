#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_dolog
----------------------------------

Tests for `dolog` module.
"""

import pytest

from contextlib import contextmanager
from click.testing import CliRunner

from dolog import dolog
from dolog import cli


@pytest.fixture
def response():
    """Sample pytest fixture.
    See more at: http://doc.pytest.org/en/latest/fixture.html
    """


def test_content(response):
    """Sample pytest test function with the pytest fixture as an argument.
    """
def test_command_line_interface():
    runner = CliRunner()
    result = runner.invoke(cli.main)
    assert result.exit_code == 0
    assert 'dolog.cli.main' in result.output
    help_result = runner.invoke(cli.main, ['--help'])
    assert help_result.exit_code == 0
    assert '--help  Show this message and exit.' in help_result.output
