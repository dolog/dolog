=====
dolog
=====

.. image:: https://badge.fury.io/py/dolog.png
    :target: http://badge.fury.io/py/dolog

``dolog`` - Tools to record and document the setup of a server or computer.

Features
--------

* TODO

==============  ==========================================================
Python support  Python 2.7, >= 3.3
Source          https://gitlab.com/dolog/dolog
Docs            https://dolog.readthedocs.io/en/latest/
Changelog       https://dolog.readthedocs.io/en/latest/history/
API             https://dolog.readthedocs.io/en/latest/api/
Issues          https://gitlab.com/dolog/dolog/issues
pypi            https://pypi.python.org/pypi/dolog
Ohloh           https://www.ohloh.net/p/dolog
git repo        .. code-block:: bash

                    $ git clone https://gitlab.com/dolog/dolog.git
install dev     .. code-block:: bash

                    $ git clone https://gitlab.com/dolog/dolog.git dolog
                    $ cd ./dolog
                    $ virtualenv .env
                    $ source .env/bin/activate
                    $ pip install -e .
tests           .. code-block:: bash

                    $ python setup.py test
==============  ==========================================================

.. _Documentation: http://dolog.readthedocs.org/en/latest/
.. _API: http://dolog.readthedocs.org/en/latest/api.html
